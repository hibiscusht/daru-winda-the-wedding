class Generics {

    resolve(page){
        if(page == "page-2" || page == "page-23"){
            return `<div class="main"><video controls><source src="assets/img/${page}.mp4" type="video/mp4"></video></div>`
        } else {
            return `<div class="main"><img src="assets/img/${page}.jpg"></div>`
        }
    }

}

const generics = new Generics()
export default generics